# 中远任务系统
go语言写的一款用于循环执行网页访问的任务系统，可以修改配置访问特定的网址。一般用于windows下的任务执行，比如自动结单，自动打印等，通过网页接口实现。比如访问http://youdomain.com/tasp.php执行一个或多个任务。

===task.ini===
任务网址配置文件，支持井号注释，如
========================================
http://youdomain.com/tasp.php?taskid=1
#http://youdomain.com/tasp.php?taskid=2
http://youdomain.com/tasp.php?taskid=3
========================================
第二个任务将不会执行

===nssm.exe===
这个是把exe包装成服务的软件，让我们的任务软件常驻内存运行



